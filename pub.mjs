import { execSync } from 'child_process';
import fs from 'fs';
const pkgMappings = {};
const projects = execSync('nx exec -- pwd')
  .toString('utf-8')
  .trim()
  .split('\n');
const nameify = name => {
  if (name.startsWith('@')) name = name.substring(1);
  name = '@expove/' + name.replace(/\//giu, '__');
  return name;
};
const nameMap = {};
const versMap = {};
const oldPkg = fs.readFileSync('package.json', 'utf-8');
const ourPkg = JSON.parse(oldPkg);
ourPkg.pnpm = {
  ...(ourPkg.pnpm ?? {}),
  overrides: {
    ...(ourPkg.pnpm?.overrides ?? {}),
  },
};

const ourHEAD = execSync(`git rev-parse --short HEAD`).toString().trim();

const cleanup = [];
process.on('exit', () => {
  fs.writeFileSync('package.json', oldPkg);
  projects.forEach(path => {
    console.log('Cleaning up', path);
    fs.writeFileSync(path + '/package.json', pkgMappings[path]);
  });
  cleanup.forEach(c => c());
});

const pkgWriteJobs = [];
projects.forEach(path => {
  const oldPkg = fs.readFileSync(path + '/package.json', 'utf-8');
  pkgMappings[path] = oldPkg;
  const pkg = JSON.parse(oldPkg);
  nameMap[pkg.name] = nameify(pkg.name);
  ourPkg.pnpm.overrides[pkg.name] = `npm:${nameMap[pkg.name]}@workspace:~`;
  const oldName = pkg.name;
  const HEAD = execSync(`git rev-parse --short HEAD`, {
    cwd: path,
  })
    .toString()
    .trim();
  const newV = pkg.version + '-upstream-' + HEAD + '-vendored-' + ourHEAD;
  versMap[oldName] = newV;
  pkgWriteJobs.push(() => {
    try {
      pkg.version = newV;
      pkg.name = nameMap[pkg.name];
      const overwrite = [];
      [
        'dependencies',
        'devDependencies',
        'peerDependencies',
        'bundleDependencies',
        'bundledDependencies',
        'optionalDependencies',
      ]
        .filter(v => pkg[v])
        .forEach(v => {
          Array.isArray(pkg[v])
            ? overwrite.push(...pkg[v].filter(z => nameMap[z]))
            : overwrite.push(...Object.keys(pkg[v]).filter(z => nameMap[z]));
        });
      console.log('Writing', pkg.name);
      fs.writeFileSync(path + '/package.json', JSON.stringify(pkg, null, 2));
      if (fs.existsSync(path + '/README.md')) {
        const oldReadme = fs.readFileSync(path + '/README.md', 'utf-8');
        cleanup.push(() => fs.writeFileSync(path + '/README.md', oldReadme));
        fs.writeFileSync(
          path + '/README.md',
          `# Vendored Package

This package is vendored! The [upstream](${encodeURI(`https://npm.im/${oldName}`)}) (${oldName}) package may differ. This package was built from [vendored](https://codeberg.org/Expo/vendored)/[${ourHEAD}](https://codeberg.org/Expo/vendored/commit/${ourHEAD}); upstream commit ${HEAD}.
${
  overwrite.length > 0
    ? `
## Unvendored Dependencies!

This package has semi-unvendored dependencies! We vendor some of the dependencies, however you will want to use the below overwrites in your \`package.json\`, as we do not alter the dependency values in package.json:

\`\`\`json
${JSON.stringify(
  {
    overrides: Object.fromEntries(
      overwrite.map(v => [v, `${nameMap[v]}@${versMap[v]}`]),
    ),
    pnpm: {
      overrides: Object.fromEntries(
        overwrite.map(v => [v, `npm:${nameMap[v]}@${versMap[v]}`]),
      ),
    },
  },
  null,
  2,
)}
\`\`\``
    : ''
}

## Pin the Version!

We can change how this is vendored at any time. It's highly recommended to pin this package to a specific version if you don't use the upstream package.

${oldReadme}`,
        );
      }
    } catch (error) {
      fs.writeFileSync(path + '/package.json', oldPkg);
      throw error;
    }
  });
});

execSync('pnpm i', {
  stdio: 'inherit',
});
execSync('nx run-many -t build --parallel 16', {
  stdio: 'inherit',
});

pkgWriteJobs.forEach(v => v());
fs.writeFileSync('package.json', JSON.stringify(ourPkg, null, 2));

projects.forEach(path => {
  console.log('Publishing', path);
  execSync('pnpm publish --access public --no-git-checks', {
    cwd: path,
    stdio: 'inherit',
  });
});
